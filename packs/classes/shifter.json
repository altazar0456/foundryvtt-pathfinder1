{
  "_id": "16mfrGi2UHTsME4W",
  "name": "Shifter",
  "type": "class",
  "img": "systems/pf1/icons/skills/blue_34.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p>Whether riding on the wind as a falcon or hiding in some fetid bog waiting to strike, the shifter is a true master of the wild. Both a devoted defender of druidic circles and a fierce predator, the shifter can take on the forms of nature and even fuse them together with devastating effect and unbridled savagery. By way of the druidic discipline of <em>wild shape</em>, they become living aspects of the wild. At first, they are able to assume only a minor aspect, but with time and practice they can fully transform into ever more powerful forms.</p>\n<p>The shifter class offers players a way to experience a shapeshifting character that is more martially inclined than a spellcasting <em>druid</em>. With each new level, the shifter’s powers grow in new and surprising ways, creating a character that thrives in battle, exploration, and stealth.</p>\n<p>Shifters are protectors of druidic circles and avengers of nature, yet a shifter’s magic is different from that of her druidic kin. Rather than invoking spells from the natural world or forging alliances with <em>animals</em>, shifters focus their supernatural powers inward to gain control over their own forms. Their ability to change their forms is as varied as the wonders of the wilds themselves but always remains at least partially rooted in the natural world. There are many paths to becoming a shifter; most are trained in that role by druidic circles and have their powers unlocked via rituals of initiation. Yet some stumble upon the gift naturally, as if their blood bore the secrets of shifter transformation.</p>\n<p>For those leaning toward the causes of law and good, the path of the shifter is one of contemplation and understanding. They become one with nature through mental and physical mimicry and gain an ever deeper spiritual understanding of the ebb and flow of the natural world. Those leaning toward the chaotic and evil teachings of druidic philosophy find such enlightenment through more violent means. These are typically quicker transformations, both brutal and painful, imparting the dark lessons of nature through its most catastrophic forms. Shifters who lean toward true neutrality are the most diverse when it comes to their command of metamorphic secrets.</p>\n<p><strong>Role</strong>: The shifter is so attuned to nature and the wild beasts of the world that she can call upon those powers to mystically fortify her being. Fluid in form and function, she can shape herself to overcome hardships and support those she befriends or serves.</p>\n<p><strong>Alignment</strong>: Any neutral.</p>\n<p><strong>Hit Die</strong>: d10.</p>\n<p><strong>Starting Wealth</strong>: 3d6x10 gp (average 105 gp).</p>\n<h2>Class Skills</h2>\n<p>The shifter’s class skills are <em>Acrobatics</em> (<em>Dex</em>), <em>Climb</em> (<em>Str</em>), <em>Craft</em> (<em>Int</em>), <em>Fly</em> (<em>Dex</em>), <em>Handle Animal</em> (<em>Cha</em>), <em>Knowledge</em> (nature) (<em>Int</em>), <em>Perception</em> (<em>Wis</em>), <em>Profession</em> (<em>Wis</em>), <em>Ride</em> (<em>Dex</em>), <em>Stealth</em> (<em>Dex</em>), <em>Survival</em> (<em>Wis</em>), and <em>Swim</em> (<em>Str</em>).</p>\n<p><strong>Skill Ranks per Level</strong>: 4 + <em>Int</em> modifier.</p>"
    },
    "tags": [],
    "changes": [],
    "links": {
      "children": [],
      "classAssociations": [
        {
          "id": "pf1.class-abilities.ph7qA9fJX3iPuqxa",
          "dataType": "compendium",
          "name": "Shifter Aspect",
          "img": "systems/pf1/icons/items/inventory/badge-tiger.jpg",
          "level": 1,
          "_index": 0
        },
        {
          "id": "pf1.class-abilities.fi5PsRq1XW7MStPM",
          "dataType": "compendium",
          "name": "Shifter Claws",
          "img": "systems/pf1/icons/items/weapons/kumade.png",
          "level": 1,
          "_index": 1
        },
        {
          "id": "pf1.class-abilities.Yr8dfM2d8JEWoYkr",
          "dataType": "compendium",
          "name": "Wild Empathy",
          "img": "systems/pf1/icons/skills/green_13.jpg",
          "level": 1,
          "_index": 2
        },
        {
          "id": "pf1.class-abilities.l52geoeGQcQz84AE",
          "dataType": "compendium",
          "name": "Defensive Instinct",
          "img": "systems/pf1/icons/skills/weapon_13.jpg",
          "level": 2,
          "_index": 3
        },
        {
          "id": "pf1.class-abilities.r8wPs0bB3WhOLxbX",
          "dataType": "compendium",
          "name": "Track",
          "img": "systems/pf1/icons/skills/beast_01.jpg",
          "level": 2,
          "_index": 4
        },
        {
          "id": "pf1.class-abilities.5iXq1igb1Cq0qobT",
          "dataType": "compendium",
          "name": "Woodland Stride",
          "img": "systems/pf1/icons/skills/green_16.jpg",
          "level": 3,
          "_index": 5
        },
        {
          "id": "pf1.class-abilities.qjpLIatoQIzmcehq",
          "dataType": "compendium",
          "name": "Wild Shape (SHI)",
          "img": "systems/pf1/icons/skills/blue_29.jpg",
          "level": 4,
          "_index": 6
        },
        {
          "id": "pf1.class-abilities.adslhIPLFgIaHBfh",
          "dataType": "compendium",
          "name": "Trackless Step",
          "img": "systems/pf1/icons/skills/green_24.jpg",
          "level": 5,
          "_index": 7
        },
        {
          "id": "pf1.class-abilities.eM1RMYR1EUBuUVTv",
          "dataType": "compendium",
          "name": "Shifter's Fury",
          "img": "systems/pf1/icons/skills/blood_06.jpg",
          "level": 6,
          "_index": 8
        },
        {
          "id": "pf1.class-abilities.jPNCx02S3ZlIPOmL",
          "dataType": "compendium",
          "name": "Chimeric Aspect",
          "img": "systems/pf1/icons/skills/blue_26.jpg",
          "level": 9,
          "_index": 9
        },
        {
          "id": "pf1.class-abilities.bXxt8L2PITMstpka",
          "dataType": "compendium",
          "name": "Greater Chimeric Aspect",
          "img": "systems/pf1/icons/skills/blue_34.jpg",
          "level": 14,
          "_index": 10
        },
        {
          "id": "pf1.class-abilities.cBwQdqQ4KmVBck3t",
          "dataType": "compendium",
          "name": "A Thousand Faces",
          "img": "systems/pf1/icons/skills/violet_03.jpg",
          "level": 18,
          "_index": 11
        },
        {
          "id": "pf1.class-abilities.5JlthJkVGEHPZypG",
          "dataType": "compendium",
          "name": "Timeless Body",
          "img": "systems/pf1/icons/skills/green_15.jpg",
          "level": 18,
          "_index": 12
        },
        {
          "id": "pf1.class-abilities.joNziZgJl9ZOJZlF",
          "dataType": "compendium",
          "name": "Final Asepct",
          "img": "systems/pf1/icons/skills/blue_17.jpg",
          "level": 20,
          "_index": 13
        }
      ]
    },
    "tag": "shifter",
    "useCustomTag": true,
    "armorProf": {
      "value": ["lgt", "med", "shl"],
      "custom": "No Metal Armor"
    },
    "weaponProf": {
      "value": [],
      "custom": "Club;Dagger;Dart;Quarterstaff;Scimitar;Scythe;Sickle;Shortspear;Sling;Spear;Natural Attacks"
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "hd": 10,
    "hp": 10,
    "bab": "high",
    "skillsPerLevel": 4,
    "savingThrows": {
      "fort": {
        "value": "high"
      },
      "ref": {
        "value": "high"
      }
    },
    "classSkills": {
      "acr": true,
      "apr": false,
      "art": true,
      "blf": false,
      "clm": true,
      "crf": true,
      "dip": false,
      "dev": false,
      "dis": false,
      "esc": false,
      "fly": true,
      "han": true,
      "hea": false,
      "int": false,
      "kar": false,
      "kdu": false,
      "ken": false,
      "kge": false,
      "khi": false,
      "klo": false,
      "kna": true,
      "kno": false,
      "kpl": false,
      "kre": false,
      "lin": false,
      "lor": true,
      "per": true,
      "prf": false,
      "pro": true,
      "rid": true,
      "sen": false,
      "slt": false,
      "spl": false,
      "ste": true,
      "sur": true,
      "swm": true,
      "umd": false
    }
  }
}
