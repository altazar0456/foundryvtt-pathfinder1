{
  "_id": "9ARmxRrYxvt0MJTo",
  "name": "Child of Yog-Sothoth",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Inherited<br><strong>Simple Template</strong> No<br><strong>Usable with Summons</strong> No<p>Creatures born of mortal flesh infused with the essence of the outer god Yog-Sothoth, these deviant children are often tasked with preparing the world for further incursions from other dimensions or agents of the Elder Mythos. Traditionally, the process of creating a child of Yog-Sothoth involves a blasphemous ritual that uses a mortal creature (typically a human) as an incubator. For the purpose of this ritual, gender is irrelevant. Giving birth to a child of Yog-Sothoth is always fatal. In most cases, the ritual results in the birth of twins—one a child of Yog-Sothoth, which can pass for a time as a member of the race of the creature in which it incubated, and one that cannot. Those twins that inherit a monstrous appearance take more after the Outer God itself in form, and are known as the spawn of Yog-Sothoth (Pathfinder RPG Bestiary 4 251).<p>“Child of Yog-Sothoth” is an inherited template that can be added to any living corporeal creature (referred to hereafter as the base creature), but typically, humanoids and animals are those chosen by the cult of Yog-Sothoth to carry the Outer God’s gifts. A child of Yog-Sothoth retains all of the base creature’s statistics and special abilities, except as listed below.<p><strong>Challenge Rating:</strong> Base creature’s CR + 1.<p><strong>Alignment:</strong> Any chaotic. The vast majority of the children of Yog-Sothoth are chaotic evil. While a good-aligned child of Yog-Sothoth is theoretically possible, such a creature would be significantly unusual in that it would need to have been separated at an early age from the cult that caused its creation, and allowed to mature with the strong guidance of a good-aligned mentor or parental figure.<p><strong>Type:</strong> The creature’s type changes to aberration (augmented). Do not recalculate its base attack bonus, saves, or skill ranks.<p><strong>Senses:</strong> The creature gains all-around vision and low-light vision.<p><strong>Armor Class:</strong> A child of Yog-Sothoth has either a +1 natural armor bonus for every 2 Hit Dice it has or the base creature’s natural armor bonus, whichever of the two leads to a higher result.<p><strong>Hit Dice:</strong> Change the creature’s racial Hit Dice to d8s. All Hit Dice derived from class levels remain unchanged.<p><strong>Defensive Abilities:</strong> A child of Yog-Sothoth gains cold resistance 10 and fire resistance 10. It has spell resistance equal to its CR + 11, and has a +4 racial bonus on saving throws against mind-affecting effects. A child of Yog-Sothoth is immune to disease and poison.<p><strong>Attacks:</strong> While the abdominal tentacles of a child of Yog-Sothoth are merely unsightly sensory organs, the sucker-shaped mouth at the tip of its tail is a primary attack that the child can use as long as it is not concealing its features (see Special Qualities below). A hit with the tail deals bite damage as normal for a creature of the child’s size (1d6 points of damage for a Medium child).<p><strong>Special Attacks:</strong> A child of Yog-Sothoth gains the following special attacks.<p><em>Blood Drain (Ex): </em>A child of Yog-Sothoth can drain blood from a grappled or helpless foe via its tail mouth, dealing 1d2 points of Constitution damage per round it does so.<p><strong>Spell-Like Abilities:</strong> A child of Yog-Sothoth gains the following spell-like abilities (the save DCs of these abilities are calculated using the child’s Intelligence score as a result of its magic savant special quality, and its caster level equals its Hit Dice): 3/day comprehend languages, detect thoughts, hypnotism; 1/day invisibility; 1/week contact entity I. A child of Yog-Sothoth with 5 Hit Dice adds contact entity II to its 1/week spell-like abilities. A child of Yog-Sothoth with 9 Hit Dice adds contact entity III to its 1/week spell-like abilities. A child of Yog-Sothoth with 13 Hit Dice adds contact entity IV to its 1/week spell-like abilities. A child of Yog-Sothoth with 17 Hit Dice adds gate to its 1/week spell-like abilities.<p><em>Stench (Su):</em> A child of Yog-Sothoth always exudes an unpleasant scent. As a swift action, the child can intensify this scent, causing it to become truly nauseating. All living creatures within 30 feet must succeed at a Fortitude saving throw (DC = 10 + 1/2 the child’s HD + the child’s Constitution modifier) or become nauseated for 1 round. The child can exude this nauseating stench for a number of rounds per day equal to its total Hit Dice, but these rounds need not be consecutive. Each round the child wishes to maintain the stench, it must use a swift action to do so. The stench is a poison effect.<p><strong>Special Qualities:</strong> A child of Yog-Sothoth gains the following special quality.<p><strong>Conceal Features:</strong> A child of Yog-Sothoth gains a +8 racial bonus on checks to disguise itself as a typical member of the base creature’s species (although it always appears as a particularly sizable member of that race) if it takes the time to don clothing or armor to hide its monstrous qualities. When it does so, it loses access to all-around vision and can’t make its tail attack.<p><strong>Magic Savant:</strong> A child of Yog-Sothoth’s intrinsic understanding of magic allows it to modify the concentration checks and save DCs of its racial spell-like abilities (whether from the base creature or from this template) that are normally affected by Charisma to be modified instead by the child’s Intelligence modifier. This doesn’t affect actual spellcasting ability, such as that granted by sorcerer levels.<p><strong>Weaknesses:</strong> A child of Yog-Sothoth gains the following weakness.<p><strong>Loathed:</strong> Children of Yog-Sothoth are loathed by animals and psychopomps. Both types of creatures gain a +4 bonus on Perception checks and Sense Motive checks against a child of Yog-Sothoth, and receive a +2 morale bonus on attack rolls and weapon damage rolls against such targets.<p><strong>Ability Scores:</strong> Str +2, Con +4, Int +4, Cha –2.<p><strong>Feats:</strong> A child of Yog-Sothoth gains Toughness as a bonus feat.<p><strong>Skills:</strong> A child of Yog-Sothoth gains a +8 racial bonus on Disguise checks to appear as a typical specimen of the base creature when it is using its conceal features ability. All Knowledge skills and Spellcraft are class skills for a child of Yog-Sothoth; a child of Yog-Sothoth gains a +4 racial bonus on Knowledge (arcana) and Spellcraft checks.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "wuhaiy8c",
        "formula": "floor(@attributes.hd.total / 2)",
        "operator": "add",
        "subTarget": "nac",
        "modifier": "base",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "5qn1qhdl",
        "formula": "2",
        "operator": "add",
        "subTarget": "str",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "e4qjyr2p",
        "formula": "4",
        "operator": "add",
        "subTarget": "con",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "muo1i3vo",
        "formula": "4",
        "operator": "add",
        "subTarget": "int",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "nz4dqfmu",
        "formula": "-2",
        "operator": "add",
        "subTarget": "cha",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "769tyzqm",
        "formula": "4",
        "operator": "add",
        "subTarget": "skill.kar",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "skill"
      },
      {
        "_id": "6e5dr8pv",
        "formula": "4",
        "operator": "add",
        "subTarget": "skill.spl",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "skill"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "1"
  }
}
