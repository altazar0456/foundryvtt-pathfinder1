{
  "_id": "JQfhDh7WlfOq9cbo",
  "name": "Psychoplasmic",
  "type": "feat",
  "img": "systems/pf1/icons/skills/violet_07.jpg",
  "sort": 0,
  "flags": {},
  "system": {
    "description": {
      "value": "<p><strong>Acquired/Inherited Template</strong> Acquired<br><strong>Simple Template</strong> No<br><strong>Usable with Summons</strong> No<p>While nearly all spirits experience the Astral Plane at some point in their cosmic journeys, it is still one of the least understood planes in the multiverse. Some say it is merely a dream of the gods, while others contend it is the realm of thought itself. Though it is a seemingly endless realm of celestial desert, creatures still roam and hunt on its infinite expanse.<p>Among the strangest are the psychoplasmic creatures that spontaneously form on the Astral Plane in no perceivable pattern. It could be that when a soul gets caught up in some form of astral eddy, its essence and the strange energies of the Astral Plane fuse to create this bizarre form of life. Some suggest that the passage of souls through the plane leaves impressions behind, the most powerful of which manifest as beings unto themselves. The most outlandish philosophies theorize that since the Astral Plane is a plane of thought, maybe a great thinker exists somewhere beyond the expanse, and these creatures are physical embodiments of its will.<p>Whatever the nature of a psychoplasmic creature’s genesis, it retains much of its knowledge of its former life, if any, but is extremely emotionally dulled. It cares nothing for its past, and instead takes on a mission or a task that it must complete, but usually without knowing why. Those who believe that the Astral Plane is both the incarnation of thought and also a thinker believe that these creatures always do the bidding of that astral mind. The evidence is scant, as there seems to be little rhyme or reason to individual psychoplasmic creatures’ actions. Instead, they seem inexplicably fixated on random things; while a few eventually change the nature of their obsessions, such refocusing is rare. Some of the most common tasks that psychoplasmic creatures propel themselves toward include hunting down beasts or beings that they never could have bested in life, exploring or drawing other creatures toward areas of the Astral Planes where few have ever tread, and giving themselves over to the service of psychic masters to whom they are inexplicably attuned.<p>The one common thread between all psychoplasmic creatures is their substance and appearance. Though they keep the forms they once held (or were molded to resemble), they seem to be composed of a large, constantly shifting clump of silvery dust. This dust continuously drifts from a psychoplasmic creature’s form, dissipating into nothing. Yet as fast as it falls, it is replaced, leaving the creature’s size unchanged even after decades or centuries of such evaporation. The strange dust of a psychoplasmic creature’s form completely disappears only after the creature fulfills its inscrutable objective.<p>Psychoplasmic creatures come in many forms. Many are large, hulking brutes that stalk those who invade or defile the Astral Plane, sometimes tracking trespassers to the Material Plane (though their mobility is diminished there). Others are more intelligent and serve as advisors and companions to shulsagas (<em>Pathfinder RPG Bestiary 4</em> 245). These beings might be treated as anything from valued members of a shulsaga community to embodiments of heroes or entire shulsaga tribes lost and reborn. Others, however, wage war on shulsagas, and are widely perceived as the psychic vengeance of deadly foes or mighty beasts from their mythology. In any case, the known power of psychoplasmic creatures has impacted shulsagas’ culture and art, and they dust many of their most sacred sites and most impressive pieces of statuary in silvery powder, to suggest suggesting the adaptability and might of psychoplasmic creatures.<p>A select few psychoplasmic creatures serve the interests of the Boneyard—though it can be difficult to tell whether that is intentional or incidental. Psychopomps keep these astral denizens at arm’s length, but nonetheless welcome their aid in their eternal vigil over the River of Souls. Many psychopomps remain skeptical of the strange creatures’ agenda, though, suspecting that these psychoplasmic allies are spying on or undermining their comrades. Regardless, no accounts tell of psychoplasmic creatures serving predators of souls, such as daemons.<p>Still other psychoplasmic creatures wander the planes, fixated on whatever was embedded in their minds when they came into being. A strange few seem to be born without any ambition beyond a nameless urge to wander the multiverse in search of adventure before they finally dissipate back into the silvery void of the Astral Plane.<p>“Psychoplasmic” is an acquired template that can be added to any corporeal creature (other than an undead), referred to hereafter as the base creature.<p><strong>Challenge Rating</strong>: Base creature’s CR + 1.<p><strong>Alignment</strong>: Usually neutral.<p><strong>Type</strong>: The creature’s type changes to outsider. Do not recalculate the base creature’s base attack bonus, saves, or skill points. It retains any subtype and gains the augmented subtype. It uses all the base creature’s statistics and special abilities except as noted in the following sections.<p><strong>Armor Class</strong>: A psychoplasmic creature gains a natural armor bonus of +2. If it already has a natural armor bonus, that bonus increases by 2.<p><strong>Hit Dice</strong>: Change all of the creature’s racial Hit Dice to d10s. All Hit Dice derived from class levels remain unchanged.<p><strong>Defensive Abilities</strong>: A psychoplasmic creature gains an amount of spell resistance equal to its CR + 5. Psychic spells bypass this spell resistance.<p><strong>Damage Reduction and Energy Resistance</strong>: A psychoplasmic creature gains damage reduction and energy resistance based on its Hit Dice, as given in the table below.\n<table>\n<tbody>\n<tr>\n<td><strong>Hit Dice</strong></td>\n<td><strong>Resist Cold, Electricity, and Fire</strong></td>\n<td><strong>DR</strong></td>\n</tr>\n<tr>\n<td>1–4</td>\n<td>5</td>\n<td>—</td>\n</tr>\n<tr>\n<td>5–10</td>\n<td>10</td>\n<td>5/magic or adamantine</td>\n</tr>\n<tr>\n<td>11+</td>\n<td>15</td>\n<td>10/magic and adamantine</td>\n</tr>\n</tbody>\n</table>\n<p><br><strong>Speed:</strong> Psychoplasmic creatures gain a fly speed of 60 feet (perfect) while on the Astral Plane.<p><strong>Attacks:</strong> A psychoplasmic creature retains all natural weapons of the base creature. It gains a slam attack that deals damage based on the ectoplasmic creature’s size.<p><strong>Special Attacks:</strong> A psychoplasmic creature retains all of the special attacks of the base creature. In addition, a psychoplasmic creature gains the following special attack.<p><em>Mindlock (Su):</em> Upon successfully making an unarmed strike or natural attack against a creature with an Intelligence score of 3 or higher, a psychoplasmic creature can attempt to impose a mindlock on that creature as a free action. The target of the mindlock must succeed at a Will saving throw (DC = 10 + 1/2 the psychoplasmic creature’s Hit Dice + its Intelligence modifier). If the target fails, it cannot cast spells, speak, or use Intelligence checks or Intelligence-based skill checks for 1 round (or 1d4 rounds, if the psychoplasmic creature has 11 Hit Dice or more).<p><strong>Ability Scores:</strong> Dex +4, Int +4.<p><strong>Skills:</strong> Survival is always a class skill for psychoplasmic creatures, and they gain a +5 racial bonus on Survival check when following tracks.<p><strong>Special Abilities: </strong>The flexible body of a psychoplasmic creature grants it the compression ability.</p>"
    },
    "tags": [],
    "actions": [],
    "attackNotes": [],
    "effectNotes": [],
    "changes": [
      {
        "_id": "uavxazrg",
        "formula": "2",
        "operator": "add",
        "subTarget": "nac",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ac"
      },
      {
        "_id": "y5o60urf",
        "formula": "4",
        "operator": "add",
        "subTarget": "dex",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      },
      {
        "_id": "z9fcqbpa",
        "formula": "4",
        "operator": "add",
        "subTarget": "int",
        "modifier": "untyped",
        "priority": 0,
        "value": 0,
        "target": "ability"
      }
    ],
    "contextNotes": [],
    "links": {
      "children": [],
      "charges": []
    },
    "armorProf": {
      "value": []
    },
    "weaponProf": {
      "value": []
    },
    "languages": {
      "value": []
    },
    "scriptCalls": [],
    "subType": "template",
    "associations": {
      "classes": []
    },
    "crOffset": "1"
  }
}
